import numpy as np
from collection import defaultdict

noPlayers = 10

dp = np.empty((3**noPlayers,2*noPlayers+1))
dp.astype(int)
dp.fill(-1)

maxPassengers = 4

cost = np.empty((2*noPlayers+1,2*noPlayers+1))
cost.astype(int)
cost.fill(10)


fact = []
curr = 1
fact.append(curr)
for i in range(1,20):
	curr = curr*i
	fact.append(curr)

# for i in range(20):
# 	print(fact[i])
cost[0][0] = 0
cost[0][1] = 1
cost[0][2] = 1
cost[0][3] = 2
cost[1][0] = 1
cost[1][1] = 0
cost[1][2] = 2
cost[1][3] = 3
cost[2][0] = 1
cost[2][1] = 2
cost[2][2] = 0
cost[2][3] = 1
cost[3][0] = 2
cost[3][1] = 3
cost[3][2] = 1
cost[3][3] = 0

priority = defaultdict(int)
discomfort = defaultdict(int)

power = np.zeros(noPlayers+1)
power.astype(int)
for i in range(noPlayers+1):
	power[i] =  int(3**i)

# for i in range(noPlayers+1):
# 	print(power[i] == 3**i)

# priority = []
# travelTime = []
#383879
cn=0
def fn(mask, currNode):
	global cn
	# if we already know the value at this state then just return it
	mask = int (mask)
	if(dp[mask][currNode] != -1):
		return dp[mask][currNode]
	cn+=1
	dp[mask][currNode] = 'Inf'

	# if all passengers have been dropped then nothing to do
	if(mask == 3**noPlayers-1):
		dp[mask][currNode] = 0
		return dp[mask][currNode]

	# Find the number of passengers currently travelling in the taxi and Drop an existing passenger
	noPassengers = 0
	tot_priority = 0
	tot_discomfort = 0

	for i in range(noPlayers):
		if( (mask/3**i)%3 == 1 ):
			tot_priority += priority[i]
			no_passengers += 1

	for i in range(noPlayers):
		if( (mask/3**i)%3 == 1 ):
			tot_discomfort += discomfort[i][noPassengers]

	for i in range(0,noPlayers):
		if( (mask/3**i)%3 == 1 ):
			noPassengers = noPassengers + 1
			dp[mask][currNode] = min( dp[mask][currNode], fn(mask+3**i,noPlayers+i+1)+cost[currNode][noPlayers+i+1] + \
				priority_cost(tot_priority, currNode,  ))
	
	# Pick up a new passenger only if # of passengers in taxi is less than maximum # of passengers allowed 
	if(noPassengers < maxPassengers):
		for i in range(0,noPlayers):
			if( (mask/3**i)%3 == 0 ):
				dp[mask][currNode] = min( dp[mask][currNode], fn(mask+3**i,i+1)+cost[currNode][i+1] )

	# Drop an existing passenger
	# for i in range(0,noPlayers):
		# if( (mask/3**i)%3 == 1 ):
			# dp[mask][currNode] = min( dp[mask][currNode], fn(mask+3**i,noPlayers+i+1)+cost[currNode][noPlayers+i+1])

	return dp[mask][currNode]


def data_initailize():

	for i in range(noPlayers):
		priority[i] = 1;
		for j in range(maxPassengers):
			discomfort[i][j+1] = j


for i in range(int(3**noPlayers)): fn(i,0)
# for i in range(int(3**noPlayers)): print(dp[i][0])
pay=[]

print(cn)
for i in range(noPlayers):
	sum=0
	for j in range(2**(noPlayers)):
		if j & (1<<i): continue
		num=0
		cnt=0
		for k in range(noPlayers):
			# if k==i: continue
			if (j&(1<<k)):
				num+=(3**k)*2
				cnt+=1
		cnt=noPlayers-cnt-1
		num = 3**noPlayers - num - 1
		# print(i, j, num)
		# print(fact[cnt]*fact[noPlayers-cnt-1]/fact[noPlayers])

		sum+=(fact[cnt]*fact[noPlayers-cnt-1]*1.0/fact[noPlayers]*(dp[num][0]-dp[num-2*(3**i)][0]))
	pay.append(-sum)

print pay