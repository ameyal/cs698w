import numpy as np
import sys
from random import randint

noPlayers = 10
maxPassengers = 4
noCities = 10000 #100x100 grid

dp = [ ([-1] * (2*noPlayers+1)) for row in range(3**noPlayers)]
src = [0]*noPlayers
dest = [0]*noPlayers
src2 = [0]*noPlayers
dest2 = [0]*noPlayers
cost = [ ([10] * (2*noPlayers+1)) for row in range(2*noPlayers+1) ]
totalCost = [0]*noPlayers
totalFare = 0

fact = []
curr = 1
fact.append(curr)
for i in range(1,20):
	curr = curr*i
	fact.append(curr)

# # for i in range(20):
# # 	print(fact[i])
# cost[0][0] = 0
# cost[0][1] = 1
# cost[0][2] = 2
# cost[0][3] = 1
# cost[0][4] = 3

# cost[1][0] = 1
# cost[1][1] = 0
# cost[1][2] = 1
# cost[1][3] = 0
# cost[1][4] = 2

# cost[2][0] = 2
# cost[2][1] = 1
# cost[2][2] = 0
# cost[2][3] = 1
# cost[2][4] = 1


# cost[3][0] = 1
# cost[3][1] = 0
# cost[3][2] = 1
# cost[3][3] = 0
# cost[3][4] = 2

# cost[4][0] = 3
# cost[4][1] = 2
# cost[4][2] = 1
# cost[4][3] = 2
# cost[4][4] = 0

def cityCost(i, j):
	x1 =i/100; y1 = i%100
	x2 = j/100; y2 = j%100
	return abs(x1-x2)+abs(y1-y2)

power = [0]*(noPlayers+1)
power[0] = 1
for i in range(noPlayers+1):
	power[i] =  3**i
# print power

# for i in range(noPlayers+1):
# 	print(power[i] == 3**i)

# priority = []
# travelTime = []
#383879

cn=0
def fn(mask, currNode):
	global cn

	# global cost
	# if we already know the value at this state then just return it
	# print mask, currNode, dp[mask][currNode], 3**noPlayers-1
	
	if(dp[mask][currNode] != -1):
		return dp[mask][currNode]
	cn+=1
	dp[mask][currNode] = sys.maxint

	# if all passengers have been dropped then nothing to do
	if(mask == 3**noPlayers-1):
		dp[mask][currNode] = cost[currNode][0]
		# print 'assign', mask, currNode, dp[mask][currNode], 3**noPlayers-1
		return dp[mask][currNode]

	# Find the number of passengers currently travelling in the taxi and *Drop an existing passenger*
	noPassengers = 0
	for i in range(noPlayers):
		# assert (mask/3**i) == (mask/3**i)
		if( (mask/power[i])%3 == 1 ):
			noPassengers = noPassengers + 1
			# print 'drop', currNode, dest[i], cost[currNode][dest[i]]
			dp[mask][currNode] = min(dp[mask][currNode], fn(mask+power[i],dest[i])+cost[currNode][dest[i]])
	
	if noPassengers > maxPassengers:
		dp[mask][currNode] = sys.maxint
		return dp[mask][currNode] 

	# Pick up a new passenger only if # of passengers in taxi is less than maximum # of passengers allowed 
	if(noPassengers < maxPassengers):
		for i in range(noPlayers):
			if( (mask/power[i])%3 == 0 ):
				# print 'pick', currNode, src[i], cost[currNode][src[i]]
				dp[mask][currNode] = min(dp[mask][currNode], fn(mask+power[i],src[i])+cost[currNode][src[i]])

	# Drop an existing passenger
	# for i in range(0,noPlayers):
	# 	if( (mask/3**i)%3 == 1 ):
	# 		dp[mask][currNode] = min( dp[mask][currNode], fn(mask+3**i,noPlayers+i+1)+cost[currNode][noPlayers+i+1])

	# print 'assign', mask, currNode, dp[mask][currNode], 3**noPlayers-1

	return dp[mask][currNode]


noSamples = 1

for sample in range(noSamples):


	for i in range(3**noPlayers):
		 for j in range(2*noPlayers+1):
		 	dp[i][j] = -1

	for i in range(noPlayers): 
		src2[i] = randint(0, noCities-1)
		dest2[i] = randint(0, noCities-1)
		src[i] = i+1
		dest[i] = noPlayers+i+1

	#calculate local cost matrix
	for i in range(2*noPlayers+1):
		for j in range(2*noPlayers+1):
			x = i; y = j;
			if i!=0:
				if i<=noPlayers: x = src2[i-1]
				else: x = dest2[i-noPlayers-1]
			if j!=0:
				if j<=noPlayers: y = src2[j-1]
				else: y = dest2[j-noPlayers-1]
			cost[i][j] = cityCost(x, y)


	for i in range(3**noPlayers): 
		fn(i,0)
		# print i, 0, dp[i][0]
	# for i in range(int(3**noPlayers)): print(dp[i][0])
	pay=[]

	print "total cost of sample", sample, dp[0][0]
	totalFare += dp[0][0]

	# print(cn)
	for i in range(noPlayers):
		sum=0
		for j in range(2**(noPlayers)):
			if j & (1<<i): continue
			#num is a subset of N\{i}, their bit is set to zero
			num=0
			cnt=0 	#number of passengers (except i) not travelling
			for k in range(noPlayers):
				# if k==i: continue
				if (j&(1<<k)):
					num+=(3**k)*2
					cnt+=1
			# cnt=noPlayers-cnt-1
			num = 3**noPlayers - num - 1
			# print('#', i, j, num)
			# print(fact[cnt]*fact[noPlayers-cnt-1]/fact[noPlayers]), dp[num][0], dp[num-2*(3**i)][0]

			sum+=(fact[cnt]*fact[noPlayers-cnt-1]*1.0/fact[noPlayers]*(dp[num][0]-dp[num-2*(power[i])][0]))
		pay.append(-sum)
		totalCost[i]+=(-sum)



	print pay, '\n'

print "Average total fare", totalFare*1.0/noSamples
print "AVerage fare of each rider"
for i in range(noPlayers): print i, totalCost[i]*1.0/noSamples