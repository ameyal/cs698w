import numpy as np
import sys
from random import randint
import operator
import math

MAX = sys.maxint
noPlayers = 6
maxPassengers = 4
noCities = 400 #100x100 grid

#dp tuple means (utility, cost) - utility is comfort + priority + cost
dp = [ ([(-1,-1)] * (2*noPlayers+1)) for row in range(3**noPlayers)]
src = [0]*noPlayers
dest = [0]*noPlayers
src2 = [0]*noPlayers
dest2 = [0]*noPlayers
cost = [ ([10] * (2*noPlayers+1)) for row in range(2*noPlayers+1) ]
total_discomfort = [0]*(3**noPlayers)
discomfort_value = [([0]*(maxPassengers+1)) for row in range(noPlayers)]
travelTime = [ ([10] * (2*noPlayers+1)) for row in range(2*noPlayers+1) ]
routeChild =  [ ([(-1,1)] * (2*noPlayers+1)) for row in range(3**noPlayers)]




fact = []
curr = 1
fact.append(curr)
for i in range(1,20):
	curr = curr*i
	fact.append(curr)

# # for i in range(20):
# # 	print(fact[i])
# cost[0][0] = 0
# cost[0][1] = 1
# cost[0][2] = 2
# cost[0][3] = 1
# cost[0][4] = 3

# cost[1][0] = 1
# cost[1][1] = 0
# cost[1][2] = 1
# cost[1][3] = 0
# cost[1][4] = 2

# cost[2][0] = 2
# cost[2][1] = 1
# cost[2][2] = 0
# cost[2][3] = 1
# cost[2][4] = 1


# cost[3][0] = 1
# cost[3][1] = 0
# cost[3][2] = 1
# cost[3][3] = 0
# cost[3][4] = 2

# cost[4][0] = 3
# cost[4][1] = 2
# cost[4][2] = 1
# cost[4][3] = 2
# cost[4][4] = 0

def cityCost(i, j):
	tmp = int(math.sqrt(noCities))
	x1 =i/tmp; y1 = i%tmp
	x2 = j/tmp; y2 = j%tmp
	return abs(x1-x2)+abs(y1-y2)

power = [0]*(noPlayers+1)
power[0] = 1
for i in range(noPlayers+1):
	power[i] =  3**i
# print power

# for i in range(noPlayers+1):
# 	print(power[i] == 3**i)

# priority = []
# travelTime = []
#383879

cn=0

def tupleAdd(x,y):
   	return tuple(map(operator.add,x,y))


def fn(mask, currNode):
	global cn

	# global cost
	# if we already know the value at this state then just return it
	# print mask, currNode, dp[mask][currNode], 3**noPlayers-1
	
	if(dp[mask][currNode] != (-1, -1)):
		return dp[mask][currNode]

	cn+=1
	dp[mask][currNode] = MAX, MAX

	# if all passengers have been dropped then nothing to do
	if(mask == 3**noPlayers-1):
		dp[mask][currNode] = (cost[currNode][0], cost[currNode][0])
		routeChild[mask][currNode] = (mask, 0)
		# print 'assign', mask, currNode, dp[mask][currNode], 3**noPlayers-1
		return dp[mask][currNode]

	# Find the number of passengers currently travelling in the taxi and *Drop an existing passenger*
	noPassengers = 0
	for i in range(noPlayers):
		# assert (mask/3**i) == (mask/3**i)
		if( (mask/power[i])%3 == 1 ):
			noPassengers = noPassengers + 1
	
	if noPassengers > maxPassengers:
		dp[mask][currNode] = sys.maxint
		return dp[mask][currNode] 

###########		DROP-PICKUP		########
	# Pick up a new passenger only if # of passengers in taxi is less than maximum # of passengers allowed 
	if(noPassengers < maxPassengers):
		for i in range(noPlayers):
			if( (mask/power[i])%3 == 0 ):
				# print 'pick', currNode, src[i], cost[currNode][src[i]]
				tmp = tupleAdd( fn(mask+power[i],src[i]), (cost[currNode][src[i]]+\
					total_discomfort[mask]*travelTime[currNode][src[i]], cost[currNode][src[i]]) )
				if tmp < dp[mask][currNode]:
					dp[mask][currNode] = tmp
					routeChild[mask][currNode] = (mask+power[i],src[i])

	# Drop an existing passenger
	for i in range(0,noPlayers):
		if( (mask/power[i])%3 == 1 ):
			# print 'drop', currNode, dest[i], cost[currNode][dest[i]]
			tmp = tupleAdd( fn(mask+power[i],dest[i]), (cost[currNode][dest[i]]+\
				total_discomfort[mask]*travelTime[currNode][dest[i]], cost[currNode][dest[i]]) )
			if tmp < dp[mask][currNode]:
				dp[mask][currNode] = tmp
				routeChild[mask][currNode] = (mask+power[i],dest[i])

	# print 'assign', mask, currNode, dp[mask][currNode], 3**noPlayers-1

	return dp[mask][currNode]


noSamples = 500

def rand_cost():
	_tmp = 0
	noSamples = 500*300
	for sample in range(noSamples):
		x = randint(0, noCities-1)
		y = randint(0, noCities-1)
		_tmp += cityCost(0,x) + cityCost(x,y) + cityCost(y,0)
	print "avg solo ride cost:", _tmp*1.0/noSamples

runs = [0, 0.00001, 0.001, 0.01, 0.05, 0.1, 0.5, 0.7, 1, 2, 3, 5, 7, 10, 20, 30, 50, 100, 200, 500, 1000, 10000, 50000, 100000] #factor by which player 0 has higher discomfort values
for run2 in range(1):
	run = 50000
	#now everything is tuple- (utility, cost)
	totalCost = [(0,0)]*noPlayers
	totalFare = 0,0
	averagePassengers = [0]*noPlayers

	for sample in range(noSamples):


		for i in range(3**noPlayers):
			 for j in range(2*noPlayers+1):
			 	dp[i][j] = (-1,-1)

		for i in range(noPlayers): 
			src2[i] = randint(0, noCities-1)
			dest2[i] = randint(0, noCities-1)
			src[i] = i+1
			dest[i] = noPlayers+i+1

		# calculate local cost matrix
		for i in range(2*noPlayers+1):
			for j in range(2*noPlayers+1):
				x = i; y = j;
				if i!=0:
					if i<=noPlayers: x = src2[i-1]
					else: x = dest2[i-noPlayers-1]
				if j!=0:
					if j<=noPlayers: y = src2[j-1]
					else: y = dest2[j-noPlayers-1]
				cost[i][j] = cityCost(x, y)

		#calculate local cost matrix, for now, time=cost (seriously?)
		for i in range(2*noPlayers+1):
			for j in range(2*noPlayers+1):
				travelTime[i][j] = cost[i][j]

		#initialize discomfort for each passenger
		for i in range(noPlayers):
			for j in range(1, maxPassengers+1):
				discomfort_value[i][j] = j*0.1 ###why is this factor 0.1? Why not anything else
		#this passenger's discomfort is varied, and changes in fare studied
		for j in range(1, maxPassengers+1):
			discomfort_value[0][j] *= run


		#normalize comfort [0, noPlayers]- divide by max, calculate total discomfort for each 
		# ma = 1.0
		# scale = noPlayers
		# for i in range(noPlayers):
		# 	for j in range(1, maxPassengers+1):
		# 		ma = max(ma, discomfort_value[i][j])
		# scale = min(scale, ma)

		# for i in range(noPlayers):
		# 	for j in range(1, maxPassengers+1):
		# 		discomfort_value[i][j] = discomfort_value[i][j]*scale/ma

		for i in range(3**noPlayers):
			
			noPassengers = 0
			discomfort = 0	
			for j in range(noPlayers):
				if (i/power[j])%3==1: noPassengers += 1
			if noPassengers > maxPassengers: continue
			for j in range(noPlayers):
				if (i/power[j])%3==1: discomfort += discomfort_value[j][noPassengers]
			total_discomfort[i] = discomfort


	############################################################
		#calculate dp
		for i in range(3**noPlayers): 
			fn(i,0)
			# print i, 0, dp[i][0]
		# for i in range(int(3**noPlayers)): print(dp[i][0])

		rideCost = dp[0][0][1]
		## print "total cost of sample", sample, ":",	 dp[0][0]
		totalFare = tupleAdd(totalFare, dp[0][0])

		# Shapley calculation- fare for each person
		pay=[]
		for i in range(noPlayers):
			sum=0
			for j in range(2**(noPlayers)):
				if j & (1<<i): continue
				#num is a subset of N\{i}, their bit is set to zero
				num=0
				cnt=0 	#number of passengers (except i) not travelling
				for k in range(noPlayers):
					# if k==i: continue
					if (j&(1<<k)):	#k is gonnu travel
						num+=(3**k)*2
						cnt+=1
				# cnt=noPlayers-cnt-1
				num = 3**noPlayers - num - 1
				# print('#', i, j, num)
				# print(fact[cnt]*fact[noPlayers-cnt-1]/fact[noPlayers]), dp[num][0], dp[num-2*(3**i)][0]

				#shapley calculated based on utility
				sum+=(fact[cnt]*fact[noPlayers-cnt-1]*1.0/fact[noPlayers]*(dp[num-2*(power[i])][0][0] - dp[num][0][0]))
			pay.append((sum,-1))

		for i in range(noPlayers):
			tmp = pay[i][0]
			tmp = tmp*1.0/dp[0][0][0]*dp[0][0][1]
			pay[i] = (pay[i][0], tmp)
			totalCost[i] = tupleAdd(totalCost[i], pay[i])
		
		#calculate how much time/discomfort each one got based on route taken. Can also trace route taken
		cur = (0,0)
		avg_passengers = [0]*noPlayers
		rideTime = 0
		tmp_time_pass = [0]*noPlayers

		while cur != (3**noPlayers-1, 0):
			tmp = routeChild[cur[0]][cur[1]]
			tmp_time = travelTime[cur[1]][tmp[1]]
			rideTime += tmp_time
			tmp_passengers = 0
			for j in range(noPlayers):
				if (cur[0]/power[j])%3 == 1:
					tmp_passengers += 1

			for j in range(noPlayers):
				if (cur[0]/power[j])%3==1:
					avg_passengers[j] += tmp_passengers*tmp_time
					tmp_time_pass[j] += tmp_time

			# print "###", cur, tmp_time, tmp_passengers
			cur = tmp

		for i in range(noPlayers):
			if tmp_time_pass[i] > 0:
				avg_passengers[i] = avg_passengers[i]*1.0/tmp_time_pass[i]

		for i in range(noPlayers):
			averagePassengers[i] += avg_passengers[i]

		## print "pay", pay
		## print "avg_passengers", avg_passengers
		## print "overall avg_passengers (running avg)", averagePassengers, '\n'

	print "factor:", run
	# print  "noPlayers:", noPlayers, "noCities:", noCities, "maxPassengers:", maxPassengers
	print "discomfort_value",discomfort_value, '\n'

	rand_cost()
	print "Average total fare", totalFare[0]*1.0/noSamples, totalFare[1]*1.0/noSamples
	# print "AVerage for of each rider- utility, cost, comfort (#co-passengers)"
	for i in range(noPlayers): print i, "%8.3f"%(totalCost[i][0]*1.0/noSamples), "%8.3f"%(totalCost[i][1]*1.0/noSamples),\
		 "%8.3f"%(averagePassengers[i]*1.0/noSamples)
	print '\n\n'